Some of the original flash's "effects" from its room definitions:

* `animate`
* `animation`
* `audiofade asset:AudioAsset target:number duration:number = 0` - Fades (or sets) volume of `asset` to `target` over `duration` seconds.
* `audioloop asset:AudioAsset` - Starts playing `asset`, set to looping.
* `audiostart` - Starts playing `asset`.
* `audiostop` - Stops playing `asset`.
* `cantunlock`
* `collisions name:ImageAsset` - Sets level's collision mask to `name``.
* `controls` - Show controls reminder. (?)
* `dance` - Start the dance minigame.
* `delay n:number`- Pause (in the trigger) for `n` seconds.
* `destination x y w h` - Condition: Player's destination, where the cursor is leading to (or where it was clicked), is in specified box.
* `entered x y w h` - Condition: Player has *left* a defined box.
* `fadein`
* `fadeout`
* `gainitem`
* `guildcheck`
* `guildload`
* `image`
* `impression`
* `intro`
* `inventory` - Activate inventory.
* `inventorylist ...items:string[]` - Set inventory to have `items`.
* `loseitem item:string` - Delete `item` from inventory array.
* `player`
* `playercursortracker`
* `playerinvisible`
* `prints`
* `quest`
* `questadd`
* `questlog`
* `questlogclear`
* `restartbutton`
* `set`
* `splishes x y w h` - Add areas for "splishes".
* `splishrate`
* `unlock`
* `viewportexpand`
* `viewporttall`
* `viewportwide`
* `weather type:string asset:ImageAsset ?:number ?:number` - Sets weather `type` and assets for it.

* `trigger`
Trigger stuff:
* `closeup ` - Activate a "closeup" mode for some item with given `asset`'s `name`d frame
* `flag name:string` - Condition: Checks if a flag is set.
* `item`
* `lightning` - Do a lightning flash.
* `once flag?:string` - Marks that trigger can be executed only once. If `flag` argument is present, then it sets all the specified flags.
* `position x y w h` - Sets a trigger's trigger box.
* `teleport room:string exit:string` - Sets current level to `room` and moves player to `exit`.
* `none\:text:string` - Shows a dialogue box with set `text`.
* `left portrait:number\:text:string` - Above, but with a `portrait` (katia_`portrait` ImageAsset) to the left.
* `right portrait:number\:text:string` - Above, but on the right.

# New definitions:
* `root type:string path:string` - Sets a `root` path of certain Asset `type` to `path`.
* `image name:string number:frames = 1` - Loads an ImageAsset from file `[name].png` from image `root`, with set amount of `frames`.
* `sound name:string` - Above, but without frames, and with AudioAsset.
* `level name:string` - Above, but LevelAsset.
* `on flag?:string` - Condition: Makes the trigger be triggered when a flag is set.
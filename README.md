# cat - cinematographic assets tosser

A game engine that tosses assets around, hopefully working in correct way to make a game.

---
All assets belong to people listed on the original game's [page](http://www.prequeladventure.com/2017/09/katia-infiltrate/).

---
TODO:
* [x] Implement (same or better) [definitions](definitions.md) =>
  * [x] Parsing framework
  * [x] `Tokens` for the parser to create from parsed output.
  * [x] Map the `Tokens` to parsing definitions (A map entry is a `{name:string, callback:(def:Definition, words:string[]) => Token}`)
* [ ] Add input handling
* [ ] Make sure viewport can be resized (like in the original game's "screen expand" sequence.
* [ ] Better level definition handling
* [ ] Make sure all code and names are consistent
* [ ] Add documentation
* [ ] Ask someone what could be added
* ?????
* More to be added

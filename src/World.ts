namespace CatWorld {
    import Box = CatEngine.Box;
    import Color = CatEngine.Color;

    //export class PhysicalBox extends CatEngine.Box implements CatEngine.Object {
    export class PhysicalBox extends CatAssets.FillBox implements CatEngine.Object {
        destinations = [];
        cleanup: boolean = true;

        // Bitmap is a collection of 16bit integers forming a 4x4 block
        //  which is checked for collisions instead of just the edge of
        //  the object that is being checked with. Though the first box
        //  that is being checked is assumed to be non-bitmapped.
        //  Something to fix later? TODO!

        bitmap: ArrayBuffer;
        bitmap_x: number;
        bitmap_y: number;
        onbump: () => void;
        onintersect: (box: PhysicalBox) => void;

        static debug_color = new Color("#FF4488", 0.2);

        constructor(x, y, w, h, public stops: boolean, system) {
            super(x, y, w, h, system, PhysicalBox.debug_color, 100);
            this.destinations.push(CatEngine.Cat.getModule("world"));
            //this.destinations.push(CatEngine.Cat.getModule("render"));
        };

        intersects(box: PhysicalBox, dx: number = 0, dy: number = 0): boolean {
            if (box.x + dx > this.x + this.w) return false; // a is left of b
            if (box.x + box.w + dx <  this.x) return false; // a is right of b
            if (box.y + dy > this.y + this.h) return false; // a is above of b
            if (box.y + box.h + dy <  this.y) return false; // a is below of b
            return true;
        }

        blocks(box: PhysicalBox, dx: number, dy: number) : boolean {
            if (!this.stops)
                return false;
            if (this.bitmap === undefined) {
                return this.intersects(box, dx, dy);
            }
            if (dx > 0)
                for (let iy = 0; iy < box.h; iy++)
                    if (this.collidesInBlock(box.x + box.w + dx, box.y + iy + dy))
                        return true;
            if (dx < 0)
                for (let iy = 0; iy < box.h; iy++)
                    if (this.collidesInBlock(box.x + dx, box.y + iy + dy))
                        return true;
            if (dy > 0)
                for (let ix = box.x; ix < box.x + box.w; ix++)
                    if (this.collidesInBlock(ix + dx, box.y + box.h + dy))
                        return true;
            if (dy < 0)
                for (let ix = box.x; ix < box.x + box.w; ix++)
                    if (this.collidesInBlock(ix + dx, box.y + dy))
                        return true;

            return false;
        }

        collidesInBlock(x, y): boolean {
            let block = this.getBlock(x, y);
            let shift = (y % 4) * 4 + (x % 4);
            return ((block >> shift) & 0x01) == 1
        }

        getBlock(x: number, y: number, block_coords: boolean = false): number {
            let block_x = block_coords ? x : Math.floor(x / 4);
            let block_y = block_coords ? y : Math.floor(y / 4);
            return new DataView(this.bitmap, ((block_y * this.bitmap_x) + block_x) * 2, 2).getUint16(0);
        }

        loadBitmap(img: CatAssets.ImageAsset, threshold: number) {
            img.promise(() => {
                this.w = img.width;
                this.h = img.height;
                this.bitmap_x = Math.ceil(img.width / 4);
                this.bitmap_y = Math.ceil(img.height / 4);
                let size = this.bitmap_x * this.bitmap_y * 2;
                this.bitmap = new ArrayBuffer(size);
                let view = new DataView(this.bitmap, 0);

                if (this.bitmap.byteLength != (size)) {
                    this.bitmap = undefined;
                    throw new Error("Failed to allocate collisions bitmap of " + Math.ceil(size / 1024) + "KB. You okay there?");
                }

                let canvas = document.createElement('canvas');
                canvas.width = img.width;
                canvas.height = img.height;
                let ctx = canvas.getContext('2d');
                ctx.imageSmoothingEnabled = false;
                ctx.drawImage(img.source, 0, 0);
                let imgData = ctx.getImageData(0, 0, img.width, img.height);

                // Iterate through all the blocks;
                for (let by = 0; by < this.bitmap_y; by++) {
                    for (let bx = 0; bx < this.bitmap_x; bx++) {
                        let block = 0xFFFF;
                        // Init block to fully "opaque"
                        // Iterate through all the block cells
                        for (let iy = 0; iy < 4; iy++) {
                            let y = by * 4 + iy;
                            for (let ix = 0; ix < 4; ix++) {
                                let x = bx * 4 + ix;
                                if (x < img.width  &&
                                    y < img.height &&
                                    imgData.data[(y * imgData.width + x) * 4 + 3] < threshold)
                                    block ^= (1 << (iy * 4 + ix));
                            }
                        }
                        view.setUint16(2 * (bx + by * this.bitmap_x), block);
                    }
                }
            });
        }
    }

    export class PlayerBox extends PhysicalBox implements CatRender.Renderable, CatInput.Clickable, CatEngine.Timed {
        cleanup: boolean = true;
        destinations = [];

        listens: string[] = ["mousedelta", "mousedown"];

        public runner: CatParser.Runner;
        public last_x;
        public last_y;
        public dest_x;
        public dest_y;
        public frame: number = 0;
        public dir;

        constructor(x: number, y: number, w: number, h: number, system: CatEngine.CoordinateSystem, public img: CatAssets.ImageAsset, public layer: number) {
            super(x, y, w, h, true, system);
            this.dest_x = x;
            this.dest_y = y;

            this.last_x = x;
            this.last_y = y;

            this.destinations.push(CatEngine.Cat.getModule("render"));
            this.destinations.push(CatEngine.Cat.getModule("world"));
            this.destinations.push(CatEngine.Cat.getModule("input"));
            this.destinations.push(new CatEngine.Timer(60));

            img.promise((p_img: CatAssets.ImageAsset) => {
                return this;
            });
        }

        render(render: CatRender.Render) {
            if (!this.img.loaded) return;

            let dst: CatEngine.Box = this.translateOrigin(render.viewport);
            render.context.drawImage(this.img.source,
                this.frame * this.img.width, 0,
                this.img.width, this.img.height,
                dst.x, dst.y + dst.h - this.img.height * render.viewport.scale,
                this.img.width * render.viewport.scale, this.img.height * render.viewport.scale);

            let cursor: CatEngine.Box = new Box(this.dest_x - 2, this.dest_y - 2, 4, 4, this.system).translateOrigin(render.viewport);
            render.context.beginPath();
            render.context.rect(cursor.x, cursor.y, cursor.w * render.viewport.scale, cursor.h * render.viewport.scale);
            render.context.fillStyle = "rgb(255, 0, 0)";
            render.context.fill();
        }

        private calculateMovementDelta(): [number, number] {
            let dx = 0;
            let dy = 0;
            if (Math.pow(this.dest_x - this.x, 2) + Math.pow(this.dest_y - this.y, 2) < 2)
                return [0 ,0];
            let angle = Math.atan2(this.dest_y - this.y, this.dest_x - this.x);
            // let segment = (angle / Math.PI + 1) * 8;
            // if ((0 <= segment && segment <= 3) || (13 <= segment && segment <= 16))
            //     dx = -1;
            // if (5 <= segment && segment <= 11)
            //     dx =  1;
            // if (1 <= segment && segment <= 7)
            //     dy = -1;
            // if (9 <= segment && segment <= 15)
            //     dy =  1;
            dx = Math.round(Math.cos(angle));
            dy = Math.round(Math.sin(angle));
            //if (dx != Math.round(Math.cos(angle)))
            //    console.log(dx, Math.cos(angle));
            //if (dy != -Math.round(Math.sin(angle)))
            //    console.log(dy, -Math.sin(angle));
            return [dx, dy];
        }

        onmousedelta(x, y): boolean {
            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            this.dest_x = x / vp.scale - CatEngine.Cat.default_coords.xOrigin;
            this.dest_y = y / vp.scale - CatEngine.Cat.default_coords.yOrigin;
            return true;
        }

        onmousedown(x, y): boolean {
            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            this.dest_x = x / vp.scale - CatEngine.Cat.default_coords.xOrigin;
            this.dest_y = y / vp.scale - CatEngine.Cat.default_coords.yOrigin;
            return true;
        }

        autostart: boolean = true;
        running: boolean = false;

        start() {
            this.running = true;
        }

        tick(timer: CatEngine.Timer) {
            if (this.running && this.runner && !this.runner.paused) {
                let render = <CatRender.Render> CatEngine.Cat.getModule("render");
                CatEngine.Cat.default_coords.xOrigin = -this.x + (render.viewport.width || 0) * 0.5;
                CatEngine.Cat.default_coords.yOrigin = -this.y + (render.viewport.height || 0)* 0.5;
                this.last_x = this.x;
                this.last_y = this.y;
                let delta = this.calculateMovementDelta();
                let world = <World> CatEngine.Cat.getModule("world");
                world.moveBox(this, delta[0], delta[1]);
                let delta2 = this.calculateMovementDelta();
                world.moveBox(this, delta2[0], delta2[1]);
            }
        }
    }

    export class World implements CatEngine.Module<PhysicalBox> {
        moduleName = "World";

        private boxes: PhysicalBox[] = [];

        constructor() {
        }

        moveBox(box: PhysicalBox, dx: number, dy: number): number {
            let intersects: number = 0;
            let canMoveX = true;
            let canMoveY = true;
            for (let checking of this.boxes) {
                if (checking == box)
                    continue;
                if (checking.blocks(box, dx, 0)) {
                    canMoveX = false;
                    if (!canMoveY)
                        break;
                }
                if (checking.blocks(box, 0, dy)) {
                    canMoveY = false;
                    if (!canMoveX)
                        break;
                }
            }
            box.x += canMoveX ? dx : 0;
            box.y += canMoveY ? dy : 0;

            if ((dx != 0 || dy != 0) && (canMoveX || canMoveY)) {
                for (let checking of this.boxes) {
                    if (checking == box)
                        continue;
                    if (checking.intersects(box) && checking.onintersect) {
                        intersects += 1;
                        checking.onintersect(box);
                    }
                }
            }
            return intersects;
        }

        addObjects(...obj: PhysicalBox[]) {
            obj.forEach((obj) => {
                this.boxes.push(obj);
            });
        };
        removeObjects(...obj: PhysicalBox[]) {
            obj.forEach((obj) =>{
                this.boxes.splice(this.boxes.indexOf(obj), 1);
            });
        };
    }
}

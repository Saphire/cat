namespace CatInput {
    export class ClickEvent {
        x: number;
        y: number;
    }

    export interface Clickable extends CatEngine.Object {
        layer: number;
        listens: string[];
        // Event handlers return whether the event should bubble further "up" the layer hirearchy. From max to 0.
        onclick?(): boolean;
        onmousedown?(x, y, event?): boolean;
        onmouseup?(): boolean;
        onmousemove?(x, y, event?): boolean;
        onmousedelta?(x, y, event?): boolean;
    }

    export class ModalClick implements Clickable {
        destinations = [];
        cleanup?: boolean;
        layer: number = 1000;
        listens = ["click", "mousedown", "mouseup", "mousemove", "mousedelta"];

        valid = false;

        resolve;
        resolver: Promise<Clickable>;
        constructor() {
            this.destinations.push(CatEngine.Cat.getModule("input"));

            this.resolver = new Promise((resolve, reject) => {
                this.resolve = resolve;
            })
        };


        onmousedown(): boolean {
            this.valid = true;
            return false;
        }

        onclick(): boolean {
            if (!this.valid)
                return true;
            this.resolve();
            CatEngine.Cat.removeObjects(this);
            return false;
        };

        promise(resolve?) {
            this.resolver.then(resolve);
        };
    }

    export class Input implements CatEngine.Module<Clickable> {
        moduleName = "Input";
        objects: CatEngine.Holder<Clickable[][]> = {};

        x: number;
        y: number;
        held: boolean = false;

        constructor(elem: HTMLElement) {
            this.createListener(elem, "mousedown");
            this.createListener(elem, "mouseup");
            this.createListener(elem, "mousemove");
            this.createListener(elem, "click");
        }

        createListener(elem: HTMLElement, type: string) {
            let render = <CatRender.Render> CatEngine.Cat.getModule("render");

            elem.addEventListener(type, (event) => {
                if (type == "mousemove" && (event instanceof MouseEvent)) {
                    let dx = this.x;
                    let dy = this.y;
                    this.x = event.clientX - render.viewport.xOrigin;
                    this.y = event.clientY - render.viewport.yOrigin;
                    dx -= this.x;
                    dy -= this.y;
                    event["dx"] = dx;
                    event["dy"] = dy;
                    render.toggleCursor(this.in_vp(render));
                    render.moveCursor(this.x, this.y);
                }
                this.onevent(type, event);
                if (type == "mousemove" && (event instanceof MouseEvent) && (event.buttons & 0x03) != 0) {
                    this.onevent("mousedelta", event);
                }
            });
        }

        onevent(type, event) {
            let render = <CatRender.Render> CatEngine.Cat.getModule("render");

            if (this.objects[type] !== undefined && this.in_vp(render)) {
                // If the event handler returns false (as in "keep propagating?"
                //   the event propagation will stop completely.
                let stop_propagation = false;
                [...this.objects[type]].reverse().forEach((layer) => {
                    if (layer === undefined)
                        return;
                    layer.forEach((obj) => {
                        if (stop_propagation)
                            return;
                        if (obj["on" + type] === undefined){
                            stop_propagation = true;
                            return;
                        }
                        // Dirty hax, but other option is to have a hardcoded mess of case or ifs
                        if (!obj["on" + type](this.x, this.y, event))
                            stop_propagation = true;
                    })
                });
                stop_propagation = false;
            }
        }

        in_vp(render) {
            let vp = render.viewport;
            return this.x >= (-vp.horizontal_shift) * vp.scale &&
                this.y >= (-vp.vertical_shift) * vp.scale &&
                this.x <= (vp.width + vp.horizontal_shift) * vp.scale &&
                this.y <= (vp.height + vp.vertical_shift) * vp.scale;
        };

        getLayer(event: string, layer: number): Clickable[] {
            if (this.objects[event] === undefined)
                this.objects[event] = [];
            if (this.objects[event][layer] === undefined)
                this.objects[event][layer] = [];
            return this.objects[event][layer];
        }

        addObjects(...obj: Clickable[]) {
            obj.forEach((obj) => {
                if (obj.listens === undefined) {
                    console.error("Object has no defined listeners!");
                    return;
                }
                obj.listens.forEach((event) => {
                    this.getLayer(event, obj.layer).push(obj);
                });
            });
        }

        removeObjects(...objects: Clickable[]) {
            objects.forEach((obj) => {
                obj.listens.forEach((event) => {
                    let l: Clickable[] = this.getLayer(event, obj.layer);
                    if (l.indexOf(obj) !== -1)
                        l.splice(l.indexOf(obj), 1);
                });
            });
        }
    }
}

namespace CatEngine {
    export class CoordinateSystem {
        constructor(public xOrigin = 0, public yOrigin = 0, public scale = 1.0) {}
    }

    export interface Module<T extends CatEngine.Object> {
        moduleName: string;

        init?(engine: CatEngine.Cat);
        initPromise?(resolve?, reject?);

        addObjects?(...obj: T[]);
        removeObjects?(...obj: T[]);
    }

    export interface Holder<V> {
        [index: string]: V;
    }

    export interface Object {
        destinations: Module<Object>[];
        cleanup?: boolean;
    }

    export class Color {
        r: number = 0;
        g: number = 0;
        b: number = 0;
        a: number = 0;
        constructor(str: string, alpha: number = 1) {
            let num = parseInt(str, 10);

            if (isNaN(num)) {
                let hash: number = str.indexOf("#") !== -1 ? 1 : 0;
                let x: number = str.indexOf("x") !== -1 ? 2 : 0;
                num = parseInt(str.substring(hash + x), 16);
            }

            this.r = ((num & 0xFF0000) >> 16);
            this.g = ((num & 0x00FF00) >> 8);
            this.b =  (num & 0x0000FF);
            this.a = alpha;
        }

        toString() {
            return "rgba("
                + Math.floor(this.r) + ","
                + Math.floor(this.g) + ","
                + Math.floor(this.b) + ","
                + this.a + ")";
        }
    }

    export class Delay {
        private cancelled: boolean = false;
        private resolver: Promise<Delay>;

        constructor(public delay: number) {
            this.resolver = new Promise((resolve, reject) => {
                setTimeout(() => {
                        if (!this.cancelled)
                            resolve(this);
                        else
                            reject(this);
                    },
                    this.delay
                )
            });
        }

        promise(resolve?: (FramedImage) => any, reject?: (FramedImage) => any) {
            return this.resolver.then(resolve, reject);
        }

        cancel() {
            this.cancelled = true;
        }
    }

    export interface Timed extends CatEngine.Object {
        running: boolean;
        length?: number; // In ticks.
        progress?: number; // In ticks!
        repeat?: boolean;

        autostart: boolean;

        tick(timer: CatEngine.Timer);
        start();
        stop?: () =>  void;
    }

    export class GenericTimer implements Timed {
        destinations = [];
        length: number;
        progress: number = 0;
        running: boolean;

        private resolver;
        private resolved = false;
        private resolve;
        private reject;

        constructor(length: number, public repeat: boolean, public autostart: boolean = true, public ticker?: (timer: Timed) => void, timer: Timer = CatEngine.Cat.timer) {
            this.length = length * timer.tps;
            this.resolver = new Promise((resolve, reject) => {
                this.resolve = resolve;
                this.reject = reject;
            });
            this.destinations.push(timer);
        }

        start() {
            this.running = true;
        }
        stop() {
            this.running = false;
            if (!this.resolved)
                this.reject();
        }

        tick(timer: CatEngine.Timer) {
            if (!this.running)
                return;
            this.progress++;
            this.ticker(this);
            if (this.progress >= this.length) {
                this.resolve();
                this.resolved = true;
                if (!this.repeat)
                    CatEngine.Cat.removeObjects(this);
                else
                    this.progress = 0;
            }
        }

        promise(resolve?, reject?) {
            this.resolver.then(resolve, reject);
        }
    }

    export class Timer implements Module<Timed> {
        private timer;
        moduleName = "Timer";

        timers: Timed[] = [];

        constructor(public tps: number, external: boolean = false) {
            if (!external)
                this.timer = setInterval(() => this.tock(), 1000/tps);
        }

        tock() {
            this.timers.forEach((timer) => {
                try { timer.tick(this) }
                catch(e) {
                    console.error(e);
                    CatEngine.Cat.removeObjects(timer);
                }
            });
        }

        addObjects(...obj: Timed[]) {
            obj.forEach((obj) => {
                if (obj.autostart)
                    obj.start();
                this.timers.push(obj);
            });
        }

        removeObjects(...obj: Timed[]) {
            obj.forEach((obj) => {
                obj.stop !== undefined ? obj.stop() : null;
                if (this.timers.indexOf(obj) !== -1)
                    this.timers.splice(this.timers.indexOf(obj), 1);
            });
        }
    }

    export class Box {
        constructor(public x, public y, public w, public h, public system: CoordinateSystem = CatEngine.Cat.default_coords) {}

        // Will translate so that it's at the same vector in both systems.
        translateOrigin(system: CoordinateSystem): Box {
            return new Box(
                system.xOrigin + (this.system.xOrigin + this.x) * system.scale / this.system.scale,
                system.yOrigin + (this.system.yOrigin + this.y) * system.scale / this.system.scale,
                (this.w * system.scale) / this.system.scale,
                (this.h * system.scale) / this.system.scale,
                system
            );
        }

        inBox(x, y): boolean {
            return (this.x <= x && x <= this.x + this.w &&
                this.y <= y && y <= this.y + this.h)
        }

        center(): [number, number] {
            return [this.x + (this.w / 2), this.y + (this.h / 2)]
        }
    }

    export class Cat {
        static static_coords: CoordinateSystem = new CoordinateSystem();
        static default_coords: CoordinateSystem = new CoordinateSystem();
        static timer: Timer = new CatEngine.Timer(20);
        static fast_timer: Timer = new CatEngine.Timer(60);
        static modules: Holder<Module<CatEngine.Object>> = {};

        static init() {
            console.log("Loading...");
            let canvas = <HTMLCanvasElement>document.getElementById("main_canvas");
            CatEngine.Cat.addModule("render", new CatRender.Render(canvas, 500, 310, 2));
            CatEngine.Cat.addModule("input", new CatInput.Input(canvas));
            CatEngine.Cat.addModule("sound", new CatSound.Sound());
            CatEngine.Cat.addModule("world", new CatWorld.World());
            CatEngine.Cat.addModule("assets", new CatAssets.Assets("assets/infiltrate.game", "assets/"));
            CatEngine.Cat.addModule("parser", new CatParser.Parser());

            for (let index in CatEngine.Cat.modules) {
                let obj = CatEngine.Cat.modules[index];
                if (obj && obj.init && obj.initPromise)
                    obj.initPromise(() => obj.init(CatEngine.Cat));
            }
        }

        static addObjects(...obj: CatEngine.Object[]) {
            obj.forEach((obj) => {
                if (obj === undefined) {
                    console.error("Invalid object!");
                    return;
                }
                if (obj.destinations === undefined) {
                    console.error("Object's destinations missing ", obj);
                    return;
                }
                obj.destinations.forEach((dst) => {
                    dst.addObjects(obj);
                });
            });
        }

        static removeObjects(...obj: CatEngine.Object[]) {
            obj.forEach((obj) =>{
                if (obj === undefined) {
                    console.error("Invalid object!");
                    return;
                }
                if (obj.destinations === undefined) {
                    console.error("Object's destinations missing ", obj);
                    return;
                }
                obj.destinations.forEach((dst) => dst.removeObjects(obj));
            });
        }

        static getModule(name) {
            if (CatEngine.Cat.modules[name] == undefined) {
                console.error("Invalid module ", name);
                return;
            }
            return CatEngine.Cat.modules[name];
        }

        static addModule(name, module: Module<Object>) {
            CatEngine.Cat.modules[name] = module;
        }
    }
}


document.addEventListener("DOMContentLoaded", CatEngine.Cat.init, false);
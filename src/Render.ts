namespace CatRender {
    class Viewport extends CatEngine.CoordinateSystem {
        public vertical_shift = 0;
        public horizontal_shift = 0;

        constructor(public xOrigin, public yOrigin, public width, public height, scale = 1.0) {
            super(xOrigin, yOrigin, scale);
        }
    }

    export interface Renderable extends CatEngine.Object {
        layer: number;
        render(render: Render): void;
    }

    export class Render implements CatEngine.Module<Renderable> {
        timer: CatEngine.Timer;
        moduleName = "Render";
        context: CanvasRenderingContext2D;
        viewport: Viewport;

        animationFrame: number;
        faded: boolean = false;

        cursor: CatAssets.ImageAsset;
        private cursor_x: 0;
        private cursor_y: 0;
        private cursor_visible: boolean = false;
        private cursor_frame: number = 0;
        private cursor_frames: number = 9;
        // Offset (in sprite size) from left top corner
        cursor_map: number[] = [
            0.0, 0.0, // Normal
            0.5, 0.0, // North and so on, 8 directions in clockwise.
            1.0, 0.0,
            1.0, 0.5,
            1.0, 1.0,
            0.5, 1.0,
            0.0, 1.0,
            0.0, 0.5,
            0.0, 0.0
        ];

        private objects: Renderable[][] = [];

        constructor(public canvas: HTMLCanvasElement, public viewWidth: number, public viewHeight: number, public scale = 1.0) {
            canvas.width = canvas.clientWidth;
            canvas.height = canvas.clientHeight;
            this.context = canvas.getContext('2d');
            this.context.imageSmoothingEnabled = false;


            // TODO: Make this less hardcoded!
            this.viewport = new Viewport(
                (canvas.width - this.viewWidth * this.scale) / 2,
                60,
                this.viewWidth,
                this.viewHeight,
                this.scale
            );

            window.addEventListener('resize', () => {
                this.canvas.width = canvas.clientWidth;
                this.canvas.height = canvas.clientHeight;
                this.context.imageSmoothingEnabled = false;

                let hor_shift = this.viewport.horizontal_shift;
                let ver_shift = this.viewport.vertical_shift;

                this.viewport = new Viewport(
                    (canvas.width - this.viewWidth * this.scale) / 2,
                    60,
                    this.viewWidth,
                    this.viewHeight,
                    this.scale
                );

                this.viewport.horizontal_shift = hor_shift;
                this.viewport.vertical_shift = ver_shift;
            });

            this.animationFrame = window.requestAnimationFrame(() => this.frame());
            this.timer = new CatEngine.Timer(0, true);
        }

        getLayer(layer: number): Renderable[] {
            if (this.objects[layer] === undefined)
                this.objects[layer] = [];
            return this.objects[layer];
        }

        addObjects(...obj: Renderable[]) {
            obj.forEach((obj) => {
                this.getLayer(obj.layer).push(obj);
            })
        }

        removeObjects(...obj: Renderable[]) {
            obj.forEach((obj) => {
                let l: Renderable[] = this.getLayer(obj.layer);
                if (l.indexOf(obj) !== -1)
                    l.splice(l.indexOf(obj), 1);
            })
        }

        toggleCursor(visibility) {
            if (this.cursor_visible != visibility) {
                this.canvas.style.cursor = visibility ? 'none' : '';
                this.cursor_visible = visibility;
            }
        }

        setCursor(frame) {
            this.cursor_frame = frame;
        }

        moveCursor(x, y) {
            this.cursor_x = x;
            this.cursor_y = y;
        }

        frame() {
            this.context.clearRect(0,0, this.canvas.width, this.canvas.height);

            let w = this.canvas.width;
            let h = this.canvas.height;

            if (!this.faded) {
                this.objects.forEach(layer => layer.forEach((image) => {
                    try {
                        image.render(this)
                    }
                    catch (e) {
                        console.error(e);
                        CatEngine.Cat.removeObjects(image);
                    }
                }));
            }
            this.timer.tock();

            if (this.cursor && this.cursor.loaded && this.cursor_visible) {
                let w = this.cursor.width;
                let h = this.cursor.height;
                this.context.drawImage(this.cursor.source,
                    this.cursor.width * this.cursor_frame, 0,
                    this.cursor.width, this.cursor.height,
                    this.cursor_x + this.viewport.xOrigin - this.cursor_map[this.cursor_frame * 2] * w,
                    this.cursor_y + this.viewport.yOrigin - this.cursor_map[this.cursor_frame * 2 + 1] * h,
                    w, h);
            }

            this.context.fillStyle = "rgb(0,0,0)";
            this.context.fillRect(
                0, 0,
                (w - (this.viewport.width * this.viewport.scale)) / 2 - this.viewport.horizontal_shift * this.viewport.scale, h
            );
            this.context.fillRect(
                this.viewport.horizontal_shift * this.viewport.scale + this.viewport.xOrigin + (this.viewport.width * this.viewport.scale), 0,
                (w - (this.viewport.width * this.viewport.scale)) / 2 - this.viewport.horizontal_shift * this.viewport.scale, h
            );
            this.context.fillRect(0, 0,
                w, this.viewport.yOrigin - this.viewport.vertical_shift);
            this.context.fillRect(0, this.viewport.yOrigin + this.viewport.height * this.viewport.scale + this.viewport.vertical_shift * this.viewport.scale,
                w, h - this.viewport.yOrigin + this.viewport.height * this.viewport.scale - this.viewport.vertical_shift * this.viewport.scale);
            requestAnimationFrame(() => this.frame());
        }
    }
}

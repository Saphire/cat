
//  Actually it deals with assets (textures, definitions, etc), which are just some data
//    and with objects that are present in the game.
//  So the name is a bit misleading.

namespace CatAssets {
    export abstract class RemoteAsset implements CatEngine.Object {
        destinations = []; // TODO Deal with this module hell.
        loaded: boolean = false;
        error: boolean = false;

        // So hax. Well, xhr is an event target, so that will do.
        source: EventTarget;
        resolver: Promise<RemoteAsset>;
        resolve;
        reject;

        protected constructor(src: EventTarget) {
            this.source = src;

            this.resolver = new Promise((resolve: (FramedImage) => any, reject?: (any) => any) => {
                this.resolve = resolve;
                this.reject = reject;
                if (!this.loaded) {
                    this.source.addEventListener('canplaythrough', () => resolve(this), false);
                    this.source.addEventListener('load', () => resolve(this), false);
                    this.source.addEventListener('error', (err) => reject(err), false);
                    this.source.addEventListener('abort', (err) => reject(err), false);
                }
                else {
                    console.error("Asset loaded before RemoteAsset's promise is made.");
                    if (!this.error)
                        return resolve(this);
                    else
                        return reject(this);
                }
            });
            this.source.addEventListener('error', () => this.error = true, false);
        }

        promise(resolve?: (FramedImage) => any, reject?: (FramedImage) => any) {
            return this.resolver.then(resolve, reject);
        }
    }

    export class Definition extends RemoteAsset implements CatParser.Parseable {
        name: string;
        tokens: CatParser.HoldingToken;
        source: XMLHttpRequest;
        data: string;
        onflag: CatEngine.Holder<CatParser.Token[]> = {};

        objects: CatEngine.Holder<CatEngine.Object> = {};
        parseVars: CatEngine.Holder<any> = {};

        constructor(public src: string) {
            super(new XMLHttpRequest());

            this.name = src.substring(src.lastIndexOf("/") + 1);

            this.resolver = this.promise((p_def: Definition) => {
                this.data = p_def.source.responseText;
                this.preParser();

                CatEngine.Cat.addObjects(this);
                this.loaded = true;

                return this;
            });
            this.resolver.catch(console.error);

            this.source.open('GET', src);
            this.source.responseType = "text";
            this.source.send();
        }

        preParser() {
            this.destinations.push(CatEngine.Cat.getModule("parser"));
        };
    }

    export class LevelAsset extends Definition {
        destinations = [];
        ready: boolean = false;

        constructor(src: string) {
            super(src);
        }
    }

    export class ImageAsset extends RemoteAsset {
        frames: number;

        width: number;
        height: number;

        source: HTMLImageElement;

        constructor(src: string, frames: number = 1.0) {
            if (frames == 0)
                throw new Error("Frames count can not be zero.");
            super(new Image());

            this.resolver = this.promise(() => {
                this.loaded = true;
                this.width = this.source.width / this.frames;
                this.height = this.source.height;

                return this;
            });
            this.resolver.catch(console.error);
            this.source.src = src;
            this.frames = frames;
        }
    }

    export class SoundAsset extends RemoteAsset implements CatSound.Playable {
        destinations = [];
        source: HTMLAudioElement;
        cleanup = true;

        constructor(src: string, public looping: boolean = false, public fadeVolume:number = 1, public fadeDelay:number = 0) {
            super(new Audio());

            this.resolver = this.promise(() => {
                this.loaded = true;

                return this;
            });
            this.resolver.catch(console.error);
            this.source.autoplay = false;
            this.source.preload = "auto";
            this.source.src = src;


            this.destinations.push(CatEngine.Cat.getModule("sound"));
        }

        play(sound: CatSound.Sound) {
            let source = sound.context.createMediaElementSource(this.source);
            source.connect(sound.context.destination);
            this.source.loop = this.looping;
            this.source.play();
        };

        fade(resolve?, reject?) {
            if (this.fadeDelay == 0) {
                this.source.volume = this.fadeVolume;
                return;
            }

            let start = this.source.volume;
            let t = new CatEngine.GenericTimer(this.fadeDelay, false, true, (ticker) => {
                this.source.volume = start - (start - this.fadeVolume) * ticker.progress / ticker.length;
            }, CatEngine.Cat.timer);
            t.promise(resolve, reject);
        }

        stop(sound: CatSound.Sound) {
            this.source.pause();
            this.source.currentTime = 0;
        }
    }

    // Objects

    export class ImageBox extends CatEngine.Box implements CatRender.Renderable {
        cleanup: boolean = true;
        destinations = [];

        public hidden: boolean = false;
        public frame: number = 0;

        constructor(x, y, system: CatEngine.CoordinateSystem, public img: CatAssets.ImageAsset, public layer: number, public flip: boolean = false) {
            super(x, y, 0, 0, system);

            img.promise((p_img: ImageAsset) => {
                this.w = p_img.width;
                this.h = p_img.height;
                return this;
            });

            this.destinations.push(CatEngine.Cat.getModule("render"));
        }

        render(render: CatRender.Render) {
            if (!this.img.loaded) return;

            let dst: CatEngine.Box = this.translateOrigin(render.viewport);
            if (this.flip) {
                render.context.scale(-1, 1);
                render.context.drawImage(this.img.source,
                    0, 0, this.w, this.h,
                    dst.x, -dst.y - dst.h, dst.w, dst.h);
                render.context.restore();
            }
            else
                render.context.drawImage(this.img.source,
                    this.frame * this.img.width, 0, this.w, this.h,
                    dst.x, dst.y, dst.w, dst.h)
        }
    }

    export class AnimatedBox extends ImageBox implements CatEngine.Timed {
        destinations = [];
        // Animation stuff!
        looping: boolean = false;
        running: boolean = true;
        length: number;
        progress: number = 0;
        autostart: boolean = true;

        private resolve;
        private resolver: Promise<Fade>;
        constructor(x, y, system: CatEngine.CoordinateSystem, public img: CatAssets.ImageAsset, fps: number, layer: number, public flip: boolean = false) {
            super(x, y, system, img, layer);

            this.destinations.push(new CatEngine.Timer(fps));
            this.destinations.push(CatEngine.Cat.getModule("render"));

            img.promise((p_img: ImageAsset) => {
                this.length = p_img.frames;
            });

            this.resolver = new Promise((resolve) => {
                this.resolve = resolve;
            });
        }

        start() {
            this.running = true;
        }

        tick(timer: CatEngine.Timer) {
            if (!this.running)
                return;

            if (this.progress < (this.img.frames - 1)) {
                this.progress += 1;
                return;
            }
            if (!this.looping) {
                this.resolve();
                return;
            }
            this.progress = 0;
        }

        stop() {
            this.resolve();
        }

        reset() {
            this.progress = 0;
            this.resolver = new Promise((resolve) => {
                this.resolve = resolve;
            });
        }

        promise(resolve?: (Fade) => any, reject?: (Fade) => any) {
            return this.resolver.then(resolve, reject);
        }

        render(render: CatRender.Render) {
            if (this.hidden) return;
            if (!this.img.loaded) return;

            let dst: CatEngine.Box = this.translateOrigin(render.viewport);

            if (this.flip) {
                render.context.scale(-1, 1);
                render.context.drawImage(this.img.source,
                    this.img.width * Math.floor(this.progress), 0, this.w, this.h,
                    -dst.x - dst.w, dst.y, dst.w, dst.h);
                render.context.scale(-1, 1);
                render.context.restore();
            }
            else
                render.context.drawImage(this.img.source,
                    this.img.width * Math.floor(this.progress), 0, this.w, this.h,
                    dst.x, dst.y, dst.w, dst.h)
        }
    }

    export class FillBox extends CatEngine.Box implements CatRender.Renderable {
        cleanup: boolean = true;
        destinations = [];
        public layer: number;

        constructor(x, y, w, h, system: CatEngine.CoordinateSystem, public color: CatEngine.Color, layer: number) {
            super(x, y, w, h, system);
            this.layer = layer;
            this.destinations.push(CatEngine.Cat.getModule("render"));
        }

        render(render: CatRender.Render) {
            let dst: CatEngine.Box = this.translateOrigin(render.viewport);
            render.context.beginPath();
            render.context.rect(dst.x - 0.5, dst.y - 0.5, dst.w + 1, dst.h + 1);
            render.context.fillStyle = this.color.toString();
            render.context.fill();
        }
    }



    export class TextBox extends CatEngine.Box implements CatRender.Renderable {
        cleanup: boolean = true;
        destinations = [];
        public layer: number;

        constructor(x, y, w, h, system: CatEngine.CoordinateSystem, public text: string[], layer: number) {
            super(x, y, w, h, system);
            this.layer = layer;
            this.destinations.push(CatEngine.Cat.getModule("render"));
        }

        render(render: CatRender.Render) {
            let dst: CatEngine.Box = this.translateOrigin(render.viewport);
            render.context.font = "20px voidpixel";
            render.context.fillStyle = "rgb(176, 179, 170)";
            let scale = render.viewport.scale;
            render.context.scale(scale, scale);

            let line = '';
            let y = 0;
            for(let n = 0; n < this.text.length; n++) {
                let testLine = line + this.text[n] + ' ';
                let metrics = render.context.measureText(testLine);
                let testWidth = metrics.width;
                if (testWidth > dst.w / scale && n > 0) {
                    render.context.fillText(line, dst.x / scale, (dst.y + y) / scale);
                    line = this.text[n] + ' ';
                    y += 13 * scale;
                }
                else {
                    line = testLine;
                }
            }
            render.context.fillText(line, dst.x / scale, (dst.y + y) / scale);

            //render.context.fillText(this.text, dst.x / scale, dst.y / scale, dst.w / scale);
            render.context.scale(1 / scale, 1 / scale);
            render.context.restore();
        }
    }

    export class Fade implements CatRender.Renderable, CatEngine.Timed {
        layer: number = 1000;
        length: number;
        progress: number = 0;
        running: boolean = false;
        autostart: boolean = true;

        destinations = [];
        resolver: Promise<Fade>;

        private resolve;
        // Direction:
        // True - fade OUT (visible to black)
        // False - fade IN (black to visible)
        constructor(seconds: number, private direction: boolean, private timer: CatEngine.Timer = CatEngine.Cat.timer) {
            this.length = Math.floor(seconds * this.timer.tps);
            this.resolver = new Promise((resolve) => {
                this.resolve = resolve;
            });

            this.destinations.push(CatEngine.Cat.timer);
            this.destinations.push(CatEngine.Cat.getModule("render"));
        }

        start() {
            if (!this.direction)
                (<CatRender.Render> CatEngine.Cat.getModule("render")).faded = false;
            this.running = true;
        }

        tick(timer: CatEngine.Timer) {
            if (this.running) {
                this.progress++;
                if (this.progress > this.length)
                    CatEngine.Cat.removeObjects(this);
            }
        }

        stop() {
            if (this.direction)
                (<CatRender.Render> CatEngine.Cat.getModule("render")).faded = true;
            this.resolve();
        }

        render(render: CatRender.Render): void {
            let alpha = this.direction ? 0 : 1;
            alpha += this.progress / this.length * (this.direction ? 1 : -1);

            render.context.beginPath();
            render.context.rect(0, 0, render.canvas.width, render.canvas.height);
            render.context.fillStyle = "rgba(0,0,0," + alpha + ")";
            render.context.fill();
        }

        promise(resolve?: (Fade) => any, reject?: (Fade) => any) {
            return this.resolver.then(resolve, reject);
        }
    }

    class SplashToken implements CatParser.Token {
        flags: string[] = [];
        delayed: boolean = false;
        holder: boolean  = false;
        paused: boolean  = false;

        image: ImageBox;

        constructor(params: CatParser.Parameters) {
            let img = <ImageAsset> (<CatAssets.Assets> CatEngine.Cat.getModule("assets")).get(params.words[0]);
            img.promise((img) => {
                // FIXME: If the viewport is (SOMEHOW) resized, this will result in misalignment.
                this.image = new ImageBox(
                    ((<CatRender.Render> CatEngine.Cat.getModule("render")).viewWidth - img.width) / 2,
                    ((<CatRender.Render> CatEngine.Cat.getModule("render")).viewHeight - img.height) / 2,
                    CatEngine.Cat.default_coords, img, Number(params.words[1]));
                this.image.cleanup = true;
            });
        }

        run(runner: CatParser.Runner) {
            runner.addObject(this.image);
        }
    }

    class FillToken implements CatParser.Token {
        flags: string[] = [];
        delayed: boolean = false;
        holder: boolean  = false;
        paused: boolean  = false;

        box: FillBox;

        constructor(params: CatParser.Parameters) {
            this.box = new FillBox(
                Number(params.words[0]), Number(params.words[1]), Number(params.words[2]), Number(params.words[3]),
                CatEngine.Cat.default_coords, new CatEngine.Color(params.words[4]), Number(params.words[5]));
        }

        run(runner: CatParser.Runner) {
            runner.addObject(this.box);
        }
    }

    export class Assets extends Definition implements CatEngine.Module<RemoteAsset> {
        moduleName = "Assets";
        total: number = 1;
        done: number = 0;

        root: CatEngine.Holder<string> = {};
        assets: CatEngine.Holder<RemoteAsset> = {};
        private preloading_parser;
        private preloading_runner: CatParser.Runner;

        constructor(src: string, root: string) {
            super(src);
            this.root["assets"] = root;
            this.initPromise = (resolve) => this.promise(() => this.done++).catch(console.error).then(resolve);
        }

        preParser() {
            this.destinations = [];
        }

        // TODO: Ideally this should jumpstart loading of Assets definition as well, but it's a bit of a mess to do
        //   when the systems themselves have not yet been initialized. Kind of an egg and a chicken problem. Yay
        //   to recursive dependencies?
        initPromise?: (any) => any;
        init() {
            this.preloading_parser = new CatParser.Parser();
            this.destinations = [this.preloading_parser];
            new CatParser.ImmideateMapper("root", (param: CatParser.Parameters) => {
                let assets = <CatAssets.Assets> param.def;
                assets.root[param.words[0]] = param.words[1];
            }, 2, 2, this.preloading_parser);
            new CatParser.ImmideateMapper("image", (param: CatParser.Parameters) => {
                let assets = <CatAssets.Assets> param.def;
                assets.set(param.words[2] || param.words[0], new CatAssets.ImageAsset(
                    assets.root["assets"] + assets.root["image"] + param.words[0] + ".png",
                    Number(param.words[1])
                ))
            }, 1, 1, this.preloading_parser);
            new CatParser.ImmideateMapper("sound", (param: CatParser.Parameters) => {
                let assets = <CatAssets.Assets> param.def;
                assets.set(param.words[0], new CatAssets.SoundAsset(
                    assets.root["assets"] + assets.root["sound"] + param.words[0] + ".mp3"
                ))
            }, 1, 1, this.preloading_parser);
            new CatParser.ImmideateMapper("level", (param: CatParser.Parameters) => {
                let assets = <CatAssets.Assets> param.def;
                assets.set(param.words[0], new CatAssets.LevelAsset(
                    assets.root["assets"] + assets.root["level"] + param.words[0] + ".room"
                ))
            }, 1, 1, this.preloading_parser);
            new CatParser.ImmideateMapper("showcursor", (param: CatParser.Parameters) => {
                let img = <ImageAsset> this.get(param.words[0]);
                img.promise((img) => {
                    let render = <CatRender.Render> CatEngine.Cat.getModule("render");
                    render.cursor = img;
                    render.toggleCursor(true);
                });
            }, 1, 1, this.preloading_parser);
            new CatParser.TokenMapper("splash", SplashToken, 2, 2, this.preloading_parser);
            new CatParser.TokenMapper("fill", FillToken, 5, 5, this.preloading_parser);
            this.preloading_parser.parse(this);
        }

        get(name: string) {
            if (this.assets[name] === undefined)
                console.error("Tried to get invalid asset [" + name +"]");
            return this.assets[name];
        }

        set(name: string, asset: RemoteAsset) {
            if (this.assets[name] !== undefined)
                throw new Error("No assets can share a name.");
            this.assets[name] = asset;
            asset.promise(() => this.preloading_check(), (err) => this.preloading_fail(err));
            this.total++;
        }

        preloading_check() {
            this.done++;
            if (this.done == this.total && this.loaded) {
                let parser = <CatParser.Parser> CatEngine.Cat.getModule("parser");
                console.log("Done loading " + this.total + " assets.");
                let count = 0;
                for (let index in parser.undefined)
                    count++;

                let total = count;
                for (let index in (<CatParser.Parser> CatEngine.Cat.getModule("parser")).maps)
                    total++;

                let t = parser.total;
                let tk = parser.total_known;
                if (count != 0) {
                    console.log("  - Out of all files, there are " + count + " (out of " + total +") undefined token names (" +
                        (count / total * 100).toFixed(0) +
                        "%)");
                    console.log("       contributing to " + (t - tk) + " (" + ((t - tk) / t * 100).toFixed(0) + "%) undefined tokens.");
                }
                if (parser.errors)
                    console.log("  - In total " + t + " (" + tk + " known) tokens with " + parser.errors + " errors.");
                this.promise(() => this.preloaded());
            }
        }

        preloading_fail(asset) {
            console.log("Failed to load: ", asset);
            this.loaded = false;
            // TODO: Fancy "Shit happened" screen?
        }

        preloaded() {
            this.preloading_runner = new CatParser.SimpleRunner(this);
            this.preloading_runner.run();

            let d = new CatEngine.Delay(1000);
            d.promise(() => {
                // Triggering the "preloaded" triggers.
                document.getElementById("preloader").style.display = "none";
                this.preloading_runner.setFlag("preloaded");
            });
        }
    }
}

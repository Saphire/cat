namespace CatParser {
    import Color = CatEngine.Color;

    export interface Parseable extends CatEngine.Object {
        name?: string;
        data: string;
        tokens: HoldingToken;
        onflag: CatEngine.Holder<Token[]>;

        objects: CatEngine.Holder<CatEngine.Object>;
        parseVars: CatEngine.Holder<any>;
    }

    export interface Token {
        name?: string;

        run(runner: Runner);

        // Paused tokens stop the execution until some event (defined by them) happens.
        paused: boolean;
        unpausePromise?(resolve);

        // Delayed tokens can be postponed until some event (defined by them) happens.
        //   When they do, they take over the Runner's execution pointer.
        delayed: boolean;
        promise?(resolve);

        // Some tokens, like `trigger`, can hold other tokens. They
        //   decide by themselves what token to give to the Runner.
        holder: boolean;
        // Used during parsing, until it return false, meaning that
        //   it will not hold any more tokens.
        addHeld?(token: CatParser.Token): boolean
        // Used during runtime, gives next token to run (either held one, or
        //   undefined, meaning that the holder ran out of tokens.
        nextHeld?(runner: Runner): Token;

        // A token might subscribe to certain flags, and will be notified if they are set.
        flags?: string[];
        flagNotify?(runner: Runner, flag: string);
    }

    export class Parameters {
        constructor(public parser: Parser, public token: string,
                    public words: string[], public def: Parseable,
                    public stack: Token[], public line: number,
                    public previous: Token, public assets: CatAssets.Assets) {}

        error(msg) {
            console.error(msg + " at " + (this.def.name || "unknown") + ":" + this.line);
            this.parser.errors += 1;
        }
    }

    export class TokenMapper<T extends Token> {
        constructor(public name: string, private c: {new (param: Parameters): T}, public argsMin: number, public argsMax = argsMin, parser?: CatParser.Parser) {
            if (parser !== undefined)
                parser.add(this);
        }
        serialize(T) {}
        deserialize(params: Parameters): T {
            return new this.c(params);
        }
    }

    // Some tokens will hold effect right after being parsed, rather than during the execution by Runner. Though
    //   the only example of such happening is during the (pre)loading.
    export class ImmideateMapper extends TokenMapper<Token> {
        constructor(public name: string, private immideate: (param: Parameters) => any, public argsMin, public argsMax = argsMin, parser?: CatParser.Parser) {
            super(name, undefined, argsMin, argsMax, parser);
        }
        serialize(any): any {};
        deserialize(param: Parameters) {
            return this.immideate(param);
        }
    }

    export class SimpleMapper extends TokenMapper<Token> {
        constructor(public name: string, private runner: (token: string, words: string[], r: Runner) => any, public argsMin, public argsMax = argsMin, parser?: CatParser.Parser) {
            super(name, undefined, argsMin, argsMax, parser);
        }
        serialize(any): any {};
        deserialize(params: Parameters): Token {
            return <Token> {
                paused: false,
                delayed: false,
                holder: false,
                run: (runner: Runner) => this.runner(params.token, params.words, runner),
            };
        }
    }

    export class HoldingToken implements Token {
        private tokens: Token[] = [];
        private pointer: number = 0;

        constructor(params: Parameters) {};
        run() {};

        delayed: boolean = false;
        holder: boolean  = true;
        paused: boolean  = false;

        addHeld(token: CatParser.Token): boolean {
            this.tokens.push(token);
            return true;
        }

        nextHeld(runner: CatParser.Runner): CatParser.Token {
            return this.tokens[this.pointer++];
        }
    }

    export class NoneToken implements Token {
        delayed: boolean = false;
        flags: string[] = [];
        holder: boolean = false;
        name: string = "";
        paused: boolean = false;

        private words: string[] = [];

        constructor(name, words) {
            this.name = name;
            this.words = words;
        }

        addHeld(token: CatParser.Token): boolean {
            return false;
        }

        run(runner: CatParser.Runner) {
            console.warn("Trying to run unknown token " + this.name + "! \"" + [this.name].concat(this.words).join(" ") + "\"")
        }

    }

    export interface Runner {
        // Flags are a singular fire-once bool object. They are only triggered when something sets it "up".
        getFlag(name: string): boolean;
        // This will set a flag up, and then will flagNotify all the subscribed tokens.
        setFlag(name: string);
        // A method to try and reset a flag. Some flags (or in some runners) might not be resettable
        resetFlag?(): boolean;

        paused: boolean;
        run();
        queryPush(token);

        addObject(obj: CatEngine.Object);
        removeObject(obj: CatEngine.Object);
        activateObject(obj: string, status: boolean);
        getObject(obj: string): CatEngine.Object;
        reset(def: Parseable);
    }

    export class SimpleRunner implements Runner {
        paused: boolean = true;

        private running: boolean = false;
        private run_trying: boolean = false;
        private stack: Token[] = [];
        private immideate_stack: Token[] = [];
        private flags: CatEngine.Holder<boolean> = {};

        constructor(private def: Parseable) {
            this.stack.push(def.tokens);
        }

        getFlag(name: string): boolean {
            if (this.flags[name] === undefined)
                return false;
            return this.flags[name];
        };

        setFlag(name: string) {
            name = name.toLowerCase();
            if (this.flags[name] == true)
                return;
            this.flags[name] = true;

            if (this.def.onflag[name] !== undefined) {
                for (let token of this.def.onflag[name]) {
                    token.flagNotify(this, name);
                    this.queryPush(token);
                }
            }
        };

        queryPush(token: Token) {
            let holder = new HoldingToken(undefined);
            holder.addHeld(token);
            this.immideate_stack.push(holder);
            if (!this.run_trying && !this.running) {
                setTimeout(() => this.run(), 0);
                this.run_trying = true;
            }
        }

        rerun() {
            setTimeout(() => this.run(), 0);
        };

        run() {
            this.running = true;
            this.run_trying = false;
            while (this.stack.length > 0 || (this.immideate_stack.length > 0 && this.stack.length == 0)) {
                if (!this.running)
                    return;

                if (!this.paused && this.immideate_stack.length > 0) {
                    this.stack.push(this.immideate_stack.splice(0, 1)[0]);
                }
                this.paused = false;

                let top = this.stack[this.stack.length - 1];
                let token = top.nextHeld(this);
                while (token === undefined) {
                    this.stack.pop();
                    top = this.stack[this.stack.length - 1];
                    if (top === undefined) {
                        this.running = false;
                        break;
                    }
                    token = top.nextHeld(this);
                }

                if (!this.running)
                    return;
                token.run(this);

                if (!this.running)
                    return;

                if (token.holder && !token.delayed)
                    this.stack.push(token);
                if (token.paused) {
                    console.log("Pausing at...", token);
                    this.running = false;
                    token.unpausePromise(() => this.rerun());
                    this.paused = true;
                    return;
                }
            }
            console.log("Stack empty, pausing.");
            this.running = false;
        }

        objects: CatEngine.Object[] = [];

        addObject(obj: CatEngine.Object) {
            this.objects.push(obj);
            CatEngine.Cat.addObjects(obj);
        }

        removing: boolean = false;
        removeObject(obj: CatEngine.Object) {
            if (this.removing) {
                console.error("Race condition... Oh fuck.");
                throw "Race condition... Oh fuck.";
            }
            this.removing = true;
            this.objects.splice(this.objects.indexOf(obj), 1);
            CatEngine.Cat.removeObjects(obj);
            this.removing = false;
        };

        activateObject(name: string, status: boolean) {
            let obj = this.def.objects[name];
            if ( status && this.objects.indexOf(obj) === -1)
                this.addObject(obj);
            if (!status && this.objects.indexOf(obj) !== -1)
                this.removeObject(obj);
        }

        getObject(name) {
            return this.def.objects[name];
        }

        reset(def: Parseable) {
            this.running = false;
            this.def = def;
            this.stack = [];
            this.immideate_stack = [];
            let filtered_objects = this.objects.filter((obj) => {
                if (obj.cleanup === true) {
                    CatEngine.Cat.removeObjects(obj);
                    return false;
                }
                return true;
            });
            delete this.objects;
            this.objects = filtered_objects;
            this.stack.push(def.tokens);
            this.rerun();
        };
    }

    export class Parser implements CatEngine.Module<Parseable> {
        moduleName = "Parser";
        undefined: CatEngine.Holder<boolean> = {};
        maps: CatEngine.Holder<TokenMapper<Token>>;
        total = 0;
        total_known = 0;
        errors = 0;

        constructor(maps?: CatEngine.Holder<TokenMapper<Token>>) {
            this.maps = maps ||  {};

            CatTokens.TokenInit.init(this);
        }

        add(map: TokenMapper<Token>, name: string = map.name) {
            this.maps[name] = map;
        }

        parse(def: Parseable) {
            // Create a stack so that we can move around it and define what holders to handle further.
            let assets = <CatAssets.Assets> CatEngine.Cat.getModule("assets");
            let last_token: Token;
            let holder_stack: Token[] = [];
            holder_stack.push(new HoldingToken(undefined));

            let lines = 0;
            def.data.split("\n").forEach((str) => {
                lines++;
                if (str === "") return;
                //let pre_words: string[] = str.trim().replace(/\s+/g, " ").split(" ");
                let words: string[] = [];
                // Some comments logic. Basically everything after `//` is discarded. Tokens starting with `/` are
                //   discarded as well.
                let pre_string: string = str.trim();
                let word = "";
                let char = "";
                for (let i: number = 0; i < pre_string.length; i++) {
                    let last_char = char;
                    char = pre_string[i];
                    if (i == 0 && char == "/") {
                        break;
                    }
                    if (char == "/" && (i < pre_string.length - 1) && pre_string[i + 1] == "/") {
                        break;
                    }
                    if (char == ":") {
                        words.push(word);
                        word = "";
                        continue;
                    }
                    if (char == " ") {
                        continue;
                    } else {
                        if (last_char == " ") {
                            words.push(word);
                            word = "";
                        }
                    }
                    word += char;
                }
                words.push(word);

                if (words.length === 0)
                    return;
                if (words.length === 1 && words[0] == "")
                    return;
                let first = words[0].toLowerCase();

                this.total += 1;
                // Check if such token exists in token map.
                if (first == "end") {
                    if (holder_stack.length > 1)
                        holder_stack.pop();
                    else
                        console.error("Trying to END but no holding blocks!");
                    return;
                }

                let token = null;
                if (this.maps[first] === undefined) {
                    if (this.undefined[first] !== true)
                        console.error("Undefined token \"" + words[0] + "\" at " + (def.name || "unknown") + ":" + lines);
                    this.undefined[first] = true;
                    token = new NoneToken(first, words.filter((v, k) => (k !== 0)));
                } else {
                    this.total_known += 1;
                    let params = new Parameters(this, first, words.filter((v, k) => (k !== 0)), def, holder_stack, lines, last_token, assets);
                    token = this.maps[first].deserialize(params);
                }

                if (token === undefined)
                    return; // Uh oh, we got the dud token! Darn.
                last_token = token;
                token.name = token.name || first;

                let held: boolean = holder_stack[holder_stack.length - 1].addHeld(token);
                while (!held) {
                    if (held == false) {
                        holder_stack.pop();
                    }
                    held = holder_stack[holder_stack.length - 1].addHeld(token);
                }

                if (token.holder)
                    holder_stack.push(token);
            });
            def.tokens = <HoldingToken> holder_stack[0];
        }

        addObjects(...obj: CatEngine.Object[]): void {
            obj.forEach((obj: CatAssets.Definition) => {
                this.parse(obj);
            })
        }

        removeObjects(...obj: CatEngine.Object[]): void {
        }
    }
}
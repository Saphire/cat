namespace CatSound {
    export interface Playable extends CatEngine.Object {
        play(sound: Sound): void;
        stop(sound: Sound): void;
    }

    export class Sound implements CatEngine.Module<Playable> {
        moduleName = "Sound";

        context: AudioContext;
        constructor() {
            this.context = new AudioContext();
        }

        addObjects(...obj: Playable[]) {
            obj.forEach((obj) => {
                obj.play(this);
            });
        };

        removeObjects(...obj: Playable[]) {
            obj.forEach((obj) =>{
                obj.stop(this);
            });
        };
    }
}

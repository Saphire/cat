namespace CatTokens {
    import Runner = CatParser.Runner;
    import Token = CatParser.Token;
    import Parameters = CatParser.Parameters;
    import TokenMapper = CatParser.TokenMapper;
    import ImmideateMapper = CatParser.ImmideateMapper;
    import SimpleMapper = CatParser.SimpleMapper;
    
    export class DelayToken implements Token {
        paused:  boolean = true;
        delayed: boolean = false;
        holder:  boolean = false;

        private readonly time: number;

        constructor(params: Parameters) {
            this.time = Number(params.words[0]);
        };

        run(runner: CatParser.Runner) {
            let delay = new CatEngine.Delay(this.time * 1000);
            this.unpausePromise = (resolve) => delay.promise(resolve);
        }

        unpausePromise?;
    }


    export class FadeToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = true;

        unpausePromise?(resolve);

        fader: CatAssets.Fade;

        constructor(params: Parameters) {
            this.fader = new CatAssets.Fade(Number(params.words[0]), params.token == "fadeout");
            this.unpausePromise = (resolver) => this.fader.promise(resolver);
        }

        run(runner: Runner) {
            runner.addObject(this.fader);
        }
    }

    export class ImageToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = false;

        image: CatAssets.ImageBox;

        constructor(params: Parameters) {
            if (params.def.parseVars["lastLayer"] === undefined) params.def.parseVars["lastLayer"] = 0;
            params.def.parseVars["lastLayer"] += 2;
            let layer = params.def.parseVars["lastLayer"];
            let image = <CatAssets.ImageAsset> params.assets.get(params.words[0]);
            image.promise(() => {
                this.image = new CatAssets.ImageBox(0, 0, CatEngine.Cat.default_coords, image, layer);
            })
        }

        run(runner: Runner) {
            runner.addObject(this.image);
        }
    }

    export class CollisionsToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = false;

        collisions: CatWorld.PhysicalBox;

        constructor(params: Parameters) {
            let image = <CatAssets.ImageAsset> params.assets.get(params.words[0]);
            image.promise(() => {
                this.collisions = new CatWorld.PhysicalBox(0, 0, 0, 0, true, CatEngine.Cat.default_coords);
                this.collisions.loadBitmap(image, 128);
            })
        }

        run(runner: Runner) {
            runner.addObject(this.collisions);
        }
    }

    export class TeleportToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = false;

        target: CatAssets.LevelAsset;
        target_name: string;
        target_exit: string;

        constructor(params: Parameters) {
            this.target = <CatAssets.LevelAsset> params.assets.get(params.words[0]);
            this.target_name = params.words[0];
            this.target_exit = params.words[1];
        }

        run(runner: Runner) {
            if (this.target === undefined) {
                throw new Error("Tried to teleport to invalid area [" + this.target_name + "].");
            }
            this.target.parseVars["teleport_exit"] = this.target_exit;
            runner.reset(this.target);
            // Move to target_exit?
        }
    }

    export class AnimationToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = false;

        name: string;
        animation: CatAssets.AnimatedBox;

        constructor(params: Parameters) {
            if (params.def.parseVars["lastLayer"] === undefined) params.def.parseVars["lastLayer"] = 0;

            let img = <CatAssets.ImageAsset> params.assets.get(params.words[2]);
            if (img === undefined) {
                params.error("No such animated image " + params.words[2]);
                return;
            }

            let animation = new CatAssets.AnimatedBox(
                Number(params.words[0]), Number(params.words[1]),
                CatEngine.Cat.default_coords, img, Number(params.words[3]), params.def.parseVars["lastLayer"] + 1
            );
            animation.cleanup = true;
            animation.looping = params.words[4] == "loop";
            params.def.objects["anim_" + (params.words[5] || params.words[2])] = animation;
            this.animation = animation;
            this.name = params.words[2];
        }

        run(runner: Runner) {
            if (this.animation == undefined) {
                console.error("Animation " + this.name + " is undefined!");
                return;
            }
            if (this.animation && this.animation.looping) {
                runner.addObject(this.animation);
            }
        }
    }

    export class AnimateToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = true;
        unpausePromise?;

        private status: boolean;
        private type: string;
        private animation: string;

        constructor(params: Parameters) {
            this.animation = params.words[0];
            let animation = (<CatAssets.AnimatedBox> params.def.objects["anim_" + this.animation]);
            if (animation === undefined) {
                params.error("No such animated image " + params.words[0]);
                return;
            }
            this.status = params.words[1] != "invisible";
            if (params.words[1] == "loop" || params.words[1] == "first" || params.words[2] == "async")
                this.paused = false;
            if (params.words[1] == "loop") {
                animation.looping = true;
            }
            if (this.status)
                this.unpausePromise = (resolver) => animation.promise(resolver);
            else
                this.paused = false;
            this.type = params.words[1];
        }

        run(runner: Runner) {
            let anim = <CatAssets.AnimatedBox> runner.getObject("anim_" + this.animation);
            if (anim === undefined) {
                console.error("No such animated image " + this.animation);
                this.paused = false;
                return;
            }

            if (this.type == "first") {
                anim.progress = 0;
                anim.reset();
                return;
            }
            runner.activateObject("anim_" + this.animation, this.status);
            if (this.status)
                anim.running = true;
        }
    }

    export class PlayerToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = false;

        params;
        layer: number;

        constructor(params: Parameters) {
            if (params.def.parseVars["lastLayer"] === undefined) params.def.parseVars["lastLayer"] = 0;

            this.params = params;
            this.layer = params.def.parseVars["lastLayer"] + 1;
            params.def.parseVars["lastLayer"] += 2;
        }

        run(runner: Runner) {
            if ((this.params.def.parseVars["teleport_exit"] || "start") != this.params.words[2])
                return;

            // TODO: DO ACTUAL THING
            // Progress! I am doing it finally!


            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            let coords = CatEngine.Cat.default_coords;
            coords.xOrigin = -Number(this.params.words[0]) + vp.width * 0.5;
            coords.yOrigin = -Number(this.params.words[1]) + vp.height* 0.5;
            let player = new CatWorld.PlayerBox(Number(this.params.words[0]), Number(this.params.words[1]), 8, 8,
                coords, <CatAssets.ImageAsset> (<CatAssets.Assets> CatEngine.Cat.getModule("assets")).get("tierra"), this.layer);

            this.params.def.objects["player"] = player;

            runner.addObject(player);
            player.runner = runner;
            // Such hax, very wow.
        }
    }

    export class DialogueToken implements Token {
        static dialogue_anim_timer = new CatEngine.Timer(8);

        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = true;

        unpausePromise?;

        params;
        layer: number;

        static box_scroll: number = 600;

        static boxl: CatAssets.ImageBox = null;
        static boxr: CatAssets.ImageBox = null;

        portrait: CatAssets.AnimatedBox;
        text: CatAssets.TextBox;

        runner: Runner;

        constructor(params: Parameters) {
            this.params = params;
            this.layer = params.def.parseVars["lastLayer"];
            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            let half = vp.width / 2;

            let splice = 0;
            let offset = 0;
            let text_x = 40;
            let multiplier = 0;
            if (params.token == "left") {
                text_x += 120;
                splice = 1;
                offset = 21;
                multiplier = 1;
            }
            if (params.token == "right") {
                splice = 1;
                offset = -21;
                multiplier = -1;
            }

            let portrait_img;
            if (params.token != "none") {
                portrait_img = params.assets.get("katia_" + params.words[0]);
                if (portrait_img === undefined)
                    params.error("No such portrait image " + params.words[0]);
            }

            // TODO: de-hardcode values and offsets.

            if (DialogueToken.boxr == null) {
                let imgl = <CatAssets.ImageAsset> params.assets.get("textbox_sprite_left");
                imgl.promise((img) => {
                    DialogueToken.boxl = new CatAssets.ImageBox(
                        half + 10 + offset - DialogueToken.box_scroll - img.width,
                        vp.height - img.height + 31, CatEngine.Cat.static_coords, img, 500);

                    if (portrait_img !== undefined && params.token == "left")
                        portrait_img.promise((img) => {
                            this.portrait = new CatAssets.AnimatedBox(
                                half + offset - multiplier * (imgl.width - 44),
                                vp.height - img.height, CatEngine.Cat.static_coords, img, 10, 501,
                                false);
                            this.portrait.looping = true;
                        });
                });
            } else {
                let imgl = DialogueToken.boxl.img;
                if (portrait_img !== undefined && params.token == "left")
                    portrait_img.promise((img) => {
                        this.portrait = new CatAssets.AnimatedBox(
                            half + offset - multiplier * (imgl.width - 44),
                            vp.height - img.height, CatEngine.Cat.static_coords, img, 10, 501,
                            false);
                        this.portrait.looping = true;
                    });
            }
            if (DialogueToken.boxr == null) {
                let imgr = <CatAssets.ImageAsset> params.assets.get("textbox_sprite_right");
                imgr.promise((img) => {
                    DialogueToken.boxr = new CatAssets.ImageBox(
                        half - 9 + offset + DialogueToken.box_scroll,
                        vp.height - img.height + 31, CatEngine.Cat.static_coords, img, 500);

                    if (portrait_img !== undefined && params.token == "right")
                        portrait_img.promise((img) => {
                            this.portrait = new CatAssets.AnimatedBox(
                                half + offset - multiplier * (imgr.width - 45) - img.width,
                                vp.height - img.height, CatEngine.Cat.static_coords, img, 10, 501,
                                true);
                            this.portrait.looping = true;
                        });
                });
            } else {
                let imgr = DialogueToken.boxr.img;
                if (portrait_img !== undefined && params.token == "right")
                    portrait_img.promise((img) => {
                        this.portrait = new CatAssets.AnimatedBox(
                            half + offset - multiplier * (imgr.width - 45) - img.width,
                            vp.height - img.height, CatEngine.Cat.static_coords, img, 10, 501,
                            true);
                        this.portrait.looping = true;
                    });
            }

            // TODO: Box shaking thing.


            params.words.splice(0, splice);
            this.text = new CatAssets.TextBox(text_x, vp.height - 98, 300, 0, CatEngine.Cat.static_coords,
                params.words, 501);
        }

        resetBoxes() {
            DialogueToken.box_scroll = 600;
            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            let half = vp.width / 2;

            let offset = 0;
            if (this.params.token == "left") {
                offset = 21;
            }
            if (this.params.token == "right") {
                offset = -21;
            }
            DialogueToken.boxl.x = half + 10 + offset - DialogueToken.box_scroll - DialogueToken.boxl.img.width;
            DialogueToken.boxl.y = vp.height - DialogueToken.boxl.img.height + 31;

            DialogueToken.boxr.x = half - 9 + offset + DialogueToken.box_scroll;
            DialogueToken.boxr.y = vp.height - DialogueToken.boxr.img.height + 31;
        }

        run(runner: Runner) {
            let click = new CatInput.ModalClick();
            this.runner = runner;
            runner.addObject(click);
            runner.addObject(DialogueToken.boxl);
            runner.addObject(DialogueToken.boxr);

            let delay = 0.15;
            let timer = CatEngine.Cat.fast_timer;
            let increment = DialogueToken.box_scroll / (delay * timer.tps);
            let scroll_timer = new CatEngine.GenericTimer(delay, false, true, () => {
                DialogueToken.box_scroll -= increment;
                DialogueToken.boxl.x += increment;
                DialogueToken.boxr.x -= increment;
                //if (this.portrait)
                //    portrait += increment * multiplier
            }, timer);

            runner.addObject(scroll_timer);
            scroll_timer.promise(() => {
                runner.addObject(this.text);
                if (this.portrait)
                    runner.addObject(this.portrait);
            });
            this.unpausePromise = (resolve) => {
                click.promise(() => {
                    resolve();
                    runner.removeObject(click);
                    runner.removeObject(DialogueToken.boxl);
                    runner.removeObject(DialogueToken.boxr);
                    runner.removeObject(scroll_timer);
                    runner.removeObject(this.text);
                    if (this.portrait)
                        runner.removeObject(this.portrait);
                    this.resetBoxes();
                });
            }
        }
    }

    export class IntroToken implements Token {
        flags:  string[] = [];
        delayed: boolean = false;
        holder:  boolean = false;
        paused:  boolean = false;

        timer: CatEngine.Timed;

        loomkatia;
        loombackground;
        katia;
        background;
        tallrain;
        runner: Runner;

        constructor(params: Parameters) {
            if (params.def.parseVars["lastLayer"] === undefined) params.def.parseVars["lastLayer"] = 0;
            let timer = CatEngine.Cat.fast_timer;
            let ticks = 0;

            let assets = <CatAssets.Assets> CatEngine.Cat.getModule("assets");
            this.loomkatia = new CatAssets.ImageBox(0, 0, CatEngine.Cat.default_coords, <CatAssets.ImageAsset> assets.get("intro_intro_loomkatia"), 1);
            this.loombackground = new CatAssets.ImageBox(0, 0, CatEngine.Cat.default_coords, <CatAssets.ImageAsset> assets.get("intro_intro_loombackground"), 1);
            this.katia = new CatAssets.ImageBox(0, 0, CatEngine.Cat.default_coords, <CatAssets.ImageAsset> assets.get("intro_intro_katia"), 4);
            this.background = new CatAssets.ImageBox(0, 0, CatEngine.Cat.default_coords, <CatAssets.ImageAsset> assets.get("intro_intro_background"), 4);
            this.tallrain = new CatAssets.ImageBox(0, 0, CatEngine.Cat.default_coords, <CatAssets.ImageAsset> assets.get("tall_rain"), 4);
            let multiplier = 2;
            this.timer = new CatEngine.GenericTimer(90, false, true, () => {
                if(ticks < 14 * 70)
                {
                    this.loomkatia.y -= (135 / 800) * multiplier;
                    this.loombackground.y -= (94 / 800) * multiplier;
                    this.katia.y = 10000;
                    this.background.y = 10000;
                    this.tallrain.y = 10000;
                }
                else if(ticks == 1000)
                {
                    this.loomkatia.y = 10000;
                    this.loombackground.y = 10000;
                }
                else if(ticks == 1300)
                {
                    this.katia.y = 0;
                    this.background.y = -190;
                    this.tallrain.y = -2690;
                }
                else if(ticks > 1300)
                {
                    this.loomkatia.y = 10000;
                    this.loombackground.y = 10000;
                    this.katia.y -= (190 / 600) * multiplier;
                    this.background.y += (190 / 600) * multiplier;
                    this.tallrain.y += 8 * multiplier;
                }
                ticks += multiplier;
            }, timer);
            params.def.objects["intro_background"] = this.background;
            params.def.objects["intro_katia"] = this.katia;
            params.def.objects["intro_tallrain"] = this.tallrain;
        }

        run(runner: Runner) {
            this.runner = runner;
            runner.addObject(this.loombackground);
            runner.addObject(this.loomkatia);
            runner.addObject(this.timer);
            runner.addObject(this.background);
            runner.addObject(this.katia);
            runner.addObject(this.tallrain);
        };
    }
    
    export class TokenInit {
        static init(parser: CatParser.Parser) {
            new TokenMapper("trigger", TriggerToken, 0, 0, parser);
            new TokenMapper("delay", DelayToken, 1, 1, parser);
            new TokenMapper("fadein", FadeToken, 1, 1, parser);
            new TokenMapper("fadeout", FadeToken, 1, 1, parser);
            new TokenMapper("image", ImageToken, 1, 1, parser);
            new TokenMapper("collisions", CollisionsToken, 1, 1, parser);
            new TokenMapper("teleport", TeleportToken, 1, 2, parser);
            new TokenMapper("animation", AnimationToken, 1, 2, parser);
            new TokenMapper("animate", AnimateToken, 1, 1, parser);
            new TokenMapper("player", PlayerToken, 1, 2, parser);
            new TokenMapper("left", DialogueToken, 1, 2, parser);
            new TokenMapper("right", DialogueToken, 1, 2, parser);
            new TokenMapper("none", DialogueToken, 1, 2, parser);
            new TokenMapper("intro", IntroToken, 1, 2, parser);

            new ImmideateMapper("item", TriggerToken.triggerItem, 1, 1, parser);
            new ImmideateMapper("once", TriggerToken.triggerOnce, 1, 1, parser);
            new ImmideateMapper("flag", TriggerToken.triggerFlag, 1, 1, parser);
            new ImmideateMapper("destination", TriggerToken.triggerDestination, 1, 1, parser);
            new ImmideateMapper("position", TriggerToken.triggerPosition, 1, 1, parser);
            new ImmideateMapper("entered", TriggerToken.triggerEntered, 1, 1, parser);
            new ImmideateMapper("on", TriggerToken.onflag, 1, 1, parser);
            new ImmideateMapper("layerfix", (params)=> {
                if (params.def.parseVars["lastLayer"] === undefined) params.def.parseVars["lastLayer"] = 0;
                params.def.parseVars["lastLayer"] += 2;
            }, 1, 1, parser);

            let assets = <CatAssets.Assets> CatEngine.Cat.getModule("assets");
            new SimpleMapper("audiostart", (token: string, words: string[], runner: Runner) => {
                runner.addObject(<CatAssets.SoundAsset> assets.get(words[0]));
            }, 1, 1, parser);
            new SimpleMapper("audioloop", (token: string, words: string[], runner: Runner) => {
                let obj = <CatAssets.SoundAsset> assets.get(words[0]);
                obj.looping = true;
                runner.addObject(obj);
            }, 1, 1, parser);
            new SimpleMapper("audiostop", (token: string, words: string[], runner: Runner) => {
                runner.removeObject(<CatAssets.SoundAsset> assets.get(words[0]));
            }, 1, 1, parser);
            new SimpleMapper("audiofade", (token: string, words: string[], runner: Runner) => {
                let obj = <CatAssets.SoundAsset> assets.get(words[0]);
                obj.fadeVolume = Number(words[1]);
                obj.fadeDelay = Number(words[2]) || 0;
                obj.fade();
            }, 1, 1, parser);
            new SimpleMapper("lightning", (token: string, words: string[], runner: Runner) => {
                let obj = new CatAssets.FillBox(0, 0, 500, 310, CatEngine.Cat.static_coords, new CatEngine.Color("#FFFFFF"), 600);
                runner.addObject(obj);
                setTimeout(() => runner.removeObject(obj), 50)
            }, 1, 1, parser);

            new SimpleMapper("set", (token, words, runner) => {
                words.forEach((flag) => runner.setFlag(flag));
            }, 1, 1, parser)
        }
    }
}
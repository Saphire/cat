namespace CatTokens {
    import Parameters = CatParser.Parameters;
    import Color = CatEngine.Color;
    import Runner = CatParser.Runner;
    import Token = CatParser.Token;
    import PhysicalBox = CatWorld.PhysicalBox;

    class DestinationBox extends CatWorld.PhysicalBox implements CatInput.Clickable {
        listens = ["mousedown", "mousedelta"];

        oninside: (box: CatWorld.PhysicalBox) => void;

        constructor(x, y, w, h, system, layer, private trigger: TriggerToken) {
            super(x, y, w, h, false, system);
            this.layer = layer;

            this.destinations.push(CatEngine.Cat.getModule("input"));
        }

        onmousedown(x, y, event?): boolean {
            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            let wx = x / vp.scale - CatEngine.Cat.default_coords.xOrigin;
            let wy = y / vp.scale - CatEngine.Cat.default_coords.yOrigin;
            if (this.inBox(wx, wy)) {
                this.trigger.ignoreLastDest = true;
                this.oninside(this);
            }
            return true;
        }

        onmousedelta(x, y): boolean {
            let vp = (<CatRender.Render> CatEngine.Cat.getModule("render")).viewport;
            let wx = x / vp.scale - CatEngine.Cat.default_coords.xOrigin;
            let wy = y / vp.scale - CatEngine.Cat.default_coords.yOrigin;
            if (this.inBox(wx, wy))
                this.oninside(this);
            return true;
        }
    }

    export class TriggerToken implements Token {
        private tokens: Token[] = [];
        private pointer: number = 0;

        constructor() {};
        private n: number = 0;
        run(runner: Runner) {
            this.n += 1;
            if (this.runner) {
                return;
            }
            if (this.position)
                runner.addObject(this.position);
            if (this.entered)
                runner.addObject(this.entered);
            if (this.destination)
                runner.addObject(this.destination);
            this.check(runner);
        }

        once: boolean = false;
        ran:  boolean = false;
        tokenFlags: CatEngine.Holder<boolean> = {};

        item: string;
        position: CatWorld.PhysicalBox;
        entered: CatWorld.PhysicalBox;
        destination: DestinationBox;

        runner: Runner;
        added: boolean = false;

        flags:  string[] = [];
        paused:  boolean = false;
        delayed: boolean = false;
        holder:  boolean = true;

        ignoreLastDest = false;
        clickedOutside = false;
        static lastDestX = 0;
        static lastDestY = 0;

        flagNotify(runner: Runner, flag: string) {
            if (this.flags.indexOf(flag) !== -1)
                this.delayed = false;
        }

        addHeld(token: CatParser.Token): boolean {
            if (token instanceof TriggerToken)
                return false;

            this.tokens.push(token);
            return true;
        }

        nextHeld(runner): CatParser.Token {
            this.ran = true;
            if (this.tokens[this.pointer] == undefined) {
                // TODO: make a check so that this runs only when needed.
                for (let flag in this.tokenFlags) {
                    runner.setFlag(flag);
                }
                if (!this.once) {
                    this.pointer = 0;
                    this.ran = false;
                    this.added = false;
                } else {
                    if (this.entered !== undefined)
                        this.runner.removeObject(this.entered);
                    if (this.position !== undefined)
                        this.runner.removeObject(this.position);
                    if (this.destination !== undefined)
                        this.runner.removeObject(this.destination);
                }
                return undefined;
            }
            return this.tokens[this.pointer++];

        }

        check(runner: Runner) {
            if (this.added && !this.delayed)
                return;
            if (runner !== undefined)
                this.runner = runner;

            if (this.item) {
                this.delayed = true;
                return;
            }
            let player = (<CatWorld.PlayerBox> this.runner.getObject("player"));
            if (this.destination) {
                if (!this.ignoreLastDest && TriggerToken.lastDestX == player.dest_x &&
                    TriggerToken.lastDestY == player.dest_y) {
                    this.delayed = true;
                    return;
                }
                this.ignoreLastDest = false;
                if (!this.destination.inBox(player.dest_x, player.dest_y)) {
                    this.delayed = true;
                    return;
                }
                this.delayed = false;
            }
            if (player) {
                TriggerToken.lastDestX = player.dest_x;
                TriggerToken.lastDestY = player.dest_y;
            }
            if (this.position) {
                if (!this.position.inBox(player.x, player.y)) {
                    this.delayed = true;
                    return;
                }
                this.delayed = false;
            }
            if (this.entered) {
                //let center = player.center();
                if (!this.entered.inBox(player.last_x, player.last_y) ||
                    this.entered.inBox(player.x, player.y)) {
                    this.delayed = true;
                    return;
                }
                this.delayed = false;
            }
            for (let flag in this.tokenFlags) {
                if (this.runner.getFlag(flag) != this.tokenFlags[flag]) {
                    this.delayed = true;
                    return;
                }
                this.delayed = false;
            }


            if (!this.added && runner === undefined) {
                this.runner.queryPush(this);
                this.added = true;
            }
        }

        oncollision(box, source: PhysicalBox) {
            if (this.position && source == this.position && this.destination) {
                let player = (<CatWorld.PlayerBox> this.runner.getObject("player"));
                if (this.position.inBox(player.last_x, player.last_y)) {
                    return;
                }
            }

            this.check(undefined);
        }

        static triggerItem(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            if (trigger.item !== undefined) {
                params.error("Trigger already has an item " + params.words[0]);
            }

            trigger.item = params.words[0].toLowerCase();
        }

        static triggerOnce(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            params.words.forEach((flag) => {
                if (trigger.tokenFlags[flag] !== undefined)
                    params.error("Trigger already has a flag " + flag);
                trigger.tokenFlags[flag] = false;
            });
            trigger.once = true;
        }

        static triggerFlag(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            params.words.forEach((flag) => {
                if (trigger.tokenFlags[flag] !== undefined)
                    params.error("Trigger already has a flag " + flag);
                trigger.tokenFlags[flag] = true;
            });
        }

        static triggerEntered(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            trigger.entered = new CatWorld.PhysicalBox(
                parseInt(params.words[0]), parseInt(params.words[1]),
                parseInt(params.words[2]), parseInt(params.words[3]),
                false, CatEngine.Cat.default_coords);
            trigger.entered.onintersect = (box) => trigger.oncollision(box, trigger.entered);
        }

        static triggerPosition(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            trigger.position = new CatWorld.PhysicalBox(
                parseInt(params.words[0]), parseInt(params.words[1]),
                parseInt(params.words[2]), parseInt(params.words[3]),
                false, CatEngine.Cat.default_coords);
            trigger.position.onintersect = (box) => trigger.oncollision(box, trigger.position);
            trigger.position.color = new Color("#88FF44",CatWorld.PhysicalBox.debug_color.a / 2)
        }

        static triggerDestination(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            trigger.destination = new DestinationBox(
                parseInt(params.words[0]), parseInt(params.words[1]),
                parseInt(params.words[2]), parseInt(params.words[3]),
                CatEngine.Cat.default_coords, 100, trigger);
            trigger.destination.oninside = (box) => {
                console.log("Mouse valid!");
                trigger.oncollision(box, trigger.destination);
            };
            trigger.destination.color = new Color("#8844FF",CatWorld.PhysicalBox.debug_color.a)
        }

        static onflag(params: Parameters) {
            let top = params.stack[params.stack.length - 1];
            if (!(top instanceof TriggerToken)) {
                params.error("Trigger condition outside of a trigger");
                return;
            }
            let trigger = <TriggerToken> top;

            // TODO: Check for duplicates!
            trigger.flags.push(params.words[0]);

            if (params.def.onflag[params.words[0]] === undefined)
                params.def.onflag[params.words[0]] = [];

            params.def.onflag[params.words[0]].push(trigger);
            trigger.delayed = true;
        }
    }
}